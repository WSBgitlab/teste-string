const restify = require("restify");

const fs = require("fs");

const path = require("path");

const {
  BotFrameworkAdapter,
  MemoryStorage,
  ConversationState,
  UserState,
} = require("botbuilder");

const { TeamsConversationBot } = require("./bots/teamsConversationBot");

// const appId = "e96321a6-2601-4738-a2af-93a2516b032e";

// const appPassword = "6E4q~eF~Y6iZlegR~4l2AeRtLM16HZQMa_";

const adapter = new BotFrameworkAdapter({
  // appId: appId,
  appId: "",
  // appPassword: appPassword,
  appPassword: "",
  ExpireAfterSeconds: 600,
});

// Create conversation and user state with in-memory storage provider.
const memoryStorage = new MemoryStorage();
const conversationState = new ConversationState(memoryStorage);
const userState = new UserState(memoryStorage);

const bot = new TeamsConversationBot(conversationState, userState);

const options = {
  certificate: fs.readFileSync(path.resolve(__dirname, "portalcrisecip.pem")),
  key: fs.readFileSync(path.resolve(__dirname, "portalcrisecip.key")),
};

const server = restify.createServer();

server.get(
  "/public/*",
  restify.plugins.serveStatic({
    directory: __dirname,
  })
);

// Listen for incoming requests.
server.post("/api/messages", (req, res) => {
  adapter.processActivity(req, res, async (context) => {
    await bot.run(context);
  });
});

server.listen(process.env.port || process.env.PORT || 3980, function () {
  console.log(`\n${server.name} listening to ${server.url}`);
});
