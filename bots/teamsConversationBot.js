const { TeamsActivityHandler, TeamsInfo } = require("botbuilder");

const path = require("path");

const ENV_FILE = path.join(__dirname, "..", ".env");

const screen = require("./service/Puppeteer");

require("dotenv").config({ path: ENV_FILE });

// The accessor names for the conversation flow and user profile state property accessors.
const CONVERSATION_FLOW_PROPERTY = "CONVERSATION_FLOW_PROPERTY";
const USER_PROFILE_PROPERTY = "USER_PROFILE_PROPERTY";

// Object to sla response userasked.
const question = {
  queue: "name_queue",
  none: "none",
};

const {
  CardFactory,
  ActivityHandler,
  MessageFactory,
  ActivityTypes,
} = require("botbuilder");

const commands = require("./../JSON/config/cmd.json");

const Help = require("./service/Help");

const Attachment = require("./service/AttachmentImages");

const sla = {
  action: false,
};

class TeamsConversationBot extends TeamsActivityHandler {
  async newProcess(context) {
    let options = [
      {
        type: "messageBack",
        title: "Novo atendimento",
        text: "Novo atendimento",
        displayText: "Desejo realizar outro atendimento.",
      },
      {
        type: "messageBack",
        title: "Encerrar Atendimento",
        text: "dialog_end",
        displayText: "Encerrar Atendimento",
      },
    ];

    await context.sendActivities([{ type: ActivityTypes.Typing }]);

    let messageCard = MessageFactory.attachment(
      CardFactory.heroCard(null, undefined, options, {
        text: "Deseja um novo atendimento? ",
      })
    );

    try {
      await context.sendActivity(messageCard);
    } catch (erro) {
      console.log(err);
    }
  }

  async help(context, message) {
    let options = [
      {
        type: "messageBack",
        title: "SLA",
        text: "dialog_sla",
        displayText: "SLA",
      },
      {
        type: "messageBack",
        title: "PCR",
        text: "dialog_pcr",
        displayText: "PCR",
      },
      {
        type: "messageBack",
        title: "R2C3",
        text: "dialog_r2c3",
        displayText: "R2C3",
      },
      {
        type: "messageBack",
        title: "STATUS AMBIENTE",
        text: "dialog_stamop",
        displayText: "STATUS AMBIENTE",
      },
    ];

    await context.sendActivities([{ type: ActivityTypes.Typing }]);

    let messageCard = MessageFactory.attachment(
      CardFactory.heroCard(null, undefined, options, { text: message })
    );

    await context.sendActivity(messageCard);

    return true;
  }

  constructor(conversationState, userState) {
    super();
    this.conversationFlow = conversationState.createProperty(
      CONVERSATION_FLOW_PROPERTY
    );
    this.userProfile = userState.createProperty(USER_PROFILE_PROPERTY);

    // The state management objects for the conversation and user.
    this.conversationState = conversationState;
    this.userState = userState;

    this.onMembersAdded(async (context, next) => {
      const memberInfo = context.activity.membersAdded;

      let membersNumber = memberInfo.length;

      console.log(memberInfo, "membersInfo");

      await next();
    });

    this.onMessage(async (context, next) => {
      let validCommands = context.activity.text.split(" ");

      console.log(sla.action, "sla var");
      if (sla.action) {
        console.log("true");
        console.log(context.activity.text);

        const queues = require("./../JSON/config/queues.json");

        await context.sendActivities([{ type: ActivityTypes.Typing }]);

        await context.sendActivity(
          "Legal! aguarde estamos validando sua fila .."
        );

        let validQueues = queues.data.filter(
          (queue) => queue === splitOpsla[1].toUpperCase()
        );

        if (validQueues.length === 0) {
          await context.sendActivities([{ type: ActivityTypes.Typing }]);
          await context.sendActivity(
            "Ops! Fila requerida não encontrada em meus arquivos .."
          );
          await context.sendActivities([{ type: ActivityTypes.Typing }]);
          await context.sendActivity("Verifique e tente novamente");
          await context.sendActivities([{ type: ActivityTypes.Typing }]);
          await context.sendActivity("Atendimento finalizado");
          return;
        }

        await context.sendActivity(
          "Aguarde em instantes sua solicitação está em processamento"
        );

        let urlSlaQueue = `https://dashboard.cip-core.local:8443/d/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&var-Queue=${splitOpsla[1].toUpperCase()}`;

        await context.sendActivities([{ type: ActivityTypes.Typing }]);

        try {
          await screen.init(context, urlSlaQueue, `sla_${splitOpsla[1]}`);
        } catch (erro) {
          console.log(erro);
        }

        await Attachment.handleOutgoingAttachment(
          context,
          path.resolve(__dirname, "..", "graphics", `sla_${splitOpsla[1]}`)
        );

        await this.newProcess(context);

        // next();
        return;
      }

      const validate = commands.cmd.filter(
        (cmdFind) => cmdFind == validCommands[0].toLowerCase()
      );

      const helpMessage = new Help(context);

      await context.sendActivities([{ type: ActivityTypes.Typing }]);

      if (validate.length === 0) {
        await this.help(context, helpMessage.sendSingle());
      }

      switch (validCommands[0].toLowerCase()) {
        case "wso":
          await this.sendWso2();
          break;
        case "prints":
          await this.photoScreen();
          break;
        case "dialog_sla":
          let optionsSla = [
            {
              type: "messageBack",
              title: "FILA SLA",
              text: "process_slaop",
              displayText:
                "Sessão SLA Fila iniciada, por favor siga as instruções!",
            },
            {
              type: "messageBack",
              title: "Centro de Comando",
              text: "process_opcdm",
              displayText:
                "Aguarde em instantes sua solicitação está em processamento",
            },
          ];

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          let messageCard = MessageFactory.attachment(
            CardFactory.heroCard(null, undefined, optionsSla, {
              text: "Selecione um serviço :",
            })
          );

          await context.sendActivity(messageCard);
          break;
        case "dialog_pcr":
          let optionsPcr = [
            {
              type: "messageBack",
              title: "PCR HEALTH CHECK",
              text: "process_pcrhc",
              displayText:
                "Aguarde em instantes sua solicitação está em processamento",
            },
            {
              type: "messageBack",
              title: "PCR Fila MQ",
              text: "process_pcrfilamq",
              displayText:
                "Aguarde em instantes sua solicitação está em processamento",
            },
            {
              type: "messageBack",
              title: "MQ Channel",
              text: "process_pcrmqchannel",
              displayText: "MQ Channel",
            },
          ];

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          let messageCardPcr = MessageFactory.attachment(
            CardFactory.heroCard(null, undefined, optionsPcr, {
              text: "Selecione um serviço sistema PCR :",
            })
          );

          await context.sendActivity(messageCardPcr);

          break;
        case "dialog_r2c3":
          let optionsRC = [
            {
              type: "messageBack",
              title: "R2C3 HEALTH CHECK",
              text: "process_r2c3hc",
              displayText: "Desejo status de Health Check.",
            },
            {
              type: "messageBack",
              title: "PROC ARQ",
              text: "dialog_env_procarq",
              displayText: "Desejo processamento de arquivos.",
            },
          ];

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          let messageCardRC = MessageFactory.attachment(
            CardFactory.heroCard(null, undefined, optionsRC, {
              text: "Selecione um serviço sistema R2C3 :",
            })
          );

          await context.sendActivity(messageCardRC);
          break;
        case "dialog_env_procarq":
          let optionsEnv = [
            {
              type: "messageBack",
              title: "HEXT",
              text: "process_r2c3_arqproc_hext",
              displayText: "Desejo o ambiente HEXT sistema ARQPROC.",
            },
            {
              type: "messageBack",
              title: "PRD",
              text: "process_r2c3_arqproc_prd",
              displayText: "Desejo o ambiente PRD sistema ARQPROC.",
            },
          ];

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          let messageCardR2c3 = MessageFactory.attachment(
            CardFactory.heroCard(null, undefined, optionsEnv, {
              text: "Selecione um ambiente:",
            })
          );

          await context.sendActivity(messageCardR2c3);
          break;
        case "process_pcrhc":
          let urlhc =
            "https://dashboard.cip-core.local:8443/d/opBSU8JMz/bot-pcr-health-check";

          let urlhc2 =
            "https://dashboard.cip-core.local:8443/d/VjtY881Gk/bot-pcr-health-check2?refresh=1m&orgId=1";

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          screen
            .init(context, urlhc, "pcrhc")
            .then((response) => console.log(response));

          await (async function () {
            await screen.init(context, urlhc2, "pcrhc2");
          })();

          await Attachment.handleOutgoingAttachment(
            context,
            path.resolve(__dirname, "..", "graphics", `pcrhc`)
          );

          await Attachment.handleOutgoingAttachment(
            context,
            path.resolve(__dirname, "..", "graphics", `pcrhc2`)
          );

          await this.newProcess(context);
          break;
        case "process_r2c3_arqproc_hext":
          let urlr2c3_hext =
            "https://dashboard.cip-core.local:8443/d/4bzcewsMz/r2c3_hext_arq_proc?refresh=1m&orgId=1";

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          await (async function () {
            try {
              await screen.init(context, urlr2c3_hext, "r2c3_arqproc_hext");
            } catch (err) {
              console.log(err);
            }
          })();

          await Attachment.handleOutgoingAttachment(
            context,
            path.resolve(__dirname, "..", "graphics", `r2c3_arqproc_hext`)
          );

          await this.newProcess(context);
          break;
        case "process_r2c3_arqproc_prd":
          let urlr2c3_prd =
            "https://dashboard.cip-core.local:8443/d/Rhe7Eo3Gk/r2c3_prod_arq_proc?refresh=1m&orgId=1";

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          await (async function () {
            try {
              await screen.init(context, urlr2c3_prd, "r2c3_arqproc_prd");
            } catch (err) {
              console.log(err);
            }
          })();

          await Attachment.handleOutgoingAttachment(
            context,
            path.resolve(__dirname, "..", "graphics", `r2c3_arqproc_prd`)
          );

          await this.newProcess(context);
          break;
        case "dialog_stamop":
          let optionsStatus = [
            {
              type: "messageBack",
              title: "PRODUTOS",
              text: "process_stam",
              displayText:
                "Aguarde em instantes sua solicitação está em processamento",
            },
            {
              type: "messageBack",
              title: "NETCOOL",
              text: "process_stamnet",
              displayText:
                "Aguarde em instantes sua solicitação está em processamento",
            },
          ];

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          let messageStatus = MessageFactory.attachment(
            CardFactory.heroCard(null, undefined, optionsStatus, {
              text: "Serviços Status Ambiente :",
            })
          );

          await context.sendActivity(messageStatus);

          break;
        case "process_stamnet":
          let urlnetcool =
            "https://172.28.14.78:8443/d/yukMMh8Gz/netcool_leandro?orgId=1";

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          await (async function () {
            try {
              await screen.init(context, urlnetcool, "netcool", true);
            } catch (err) {
              console.log(err);
            }
          })();

          await Attachment.handleOutgoingAttachment(
            context,
            path.resolve(__dirname, "..", "graphics", `netcool`)
          );

          await this.newProcess(context);

          break;
        case "process_stam":
          let urlstam =
            "https://dashboard.cip-core.local:8443/d/4uWyLnxGz/rundeck-status-ambiente?refresh=1m&orgId=1";

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          await (async function () {
            try {
              await screen.init(context, urlstam, "stamb");
            } catch (err) {
              console.log(err);
            }
          })();

          await Attachment.handleOutgoingAttachment(
            context,
            path.resolve(__dirname, "..", "graphics", `stamb`)
          );

          await this.newProcess(context);
          break;
        case "process_pcrfilamq":
          let urlPcrFila =
            "https://dashboard.cip-core.local:8443/d/dY58vDmZk/enfileiramento_mq_msg_part_in?orgId=1&https:%2F%2Fdashboard.cip-core.local:8443%2Fd%2FdY58vDmZk%2Fenfileiramento_mq_msg_part_in%3ForgId=1";

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          try {
            await screen.init(context, urlPcrFila, "pcrfilamq");
          } catch (erro) {
            console.log("Erro pcr fila MQ", erro);
          }

          await Attachment.handleOutgoingAttachment(
            context,
            path.resolve(__dirname, "..", "graphics", `pcrfilamq`)
          );

          await this.newProcess(context);
          break;
        case "process_pcrmqchannel":
          let urlPcrChannel =
            "https://dashboard.cip-core.local:8443/d/_rpTdZ7iz/pcr-mq-channel-performance?refresh=1m&orgId=1";

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          try {
            await screen.init(context, urlPcrChannel, "pcrmqchannel");
          } catch (erro) {
            console.log("Erro pcr MQ Channel");
          }

          await Attachment.handleOutgoingAttachment(
            context,
            path.resolve(__dirname, "..", "graphics", `pcrmqchannel`)
          );

          await this.newProcess(context);
          break;
        case "process_r2c3hc":
          let urlR2c3HC =
            "https://dashboard.cip-core.local:8443/d/DBnNz4FGz/r2c3-hext?refresh=1m&orgId=1";

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          try {
            await screen.init(context, urlR2c3HC, "r2c3hc");
          } catch (erro) {
            console.log("Erro ao realizar o download", erro);
          }

          await Attachment.handleOutgoingAttachment(
            context,
            path.resolve(__dirname, "..", "graphics", `r2c3hc`)
          );

          await this.newProcess(context);
          break;
        case "process_opcdm":
          let url = `https://dashboard.cip-core.local:8443/d/L2b_kbfWz/sla_central_prd_centro_comando`;

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          try {
            await screen.init(context, url, `opcdm`);
          } catch (erro) {
            console.log("Erro ao realizar o download", erro);
          }

          await Attachment.handleOutgoingAttachment(
            context,
            path.resolve(__dirname, "..", "graphics", `opcdm`)
          );

          await this.newProcess(context);
          break;
        case "opsla":
          const queues = require("./../JSON/config/queues.json");

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          await context.sendActivity(
            "Legal! aguarde estamos validando sua fila .."
          );

          let validQueues = queues.data.filter(
            (queue) => queue === splitOpsla[1].toUpperCase()
          );

          if (validQueues.length === 0) {
            await context.sendActivities([{ type: ActivityTypes.Typing }]);
            await context.sendActivity(
              "Ops! Fila requerida não encontrada em meus arquivos .."
            );
            await context.sendActivities([{ type: ActivityTypes.Typing }]);
            await context.sendActivity("Verifique e tente novamente");
            await context.sendActivities([{ type: ActivityTypes.Typing }]);
            await context.sendActivity("Atendimento finalizado");
            return;
          }

          await context.sendActivity(
            "Aguarde em instantes sua solicitação está em processamento"
          );

          let urlSlaQueue = `https://dashboard.cip-core.local:8443/d/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&var-Queue=${splitOpsla[1].toUpperCase()}`;

          await context.sendActivities([{ type: ActivityTypes.Typing }]);

          try {
            await screen.init(context, urlSlaQueue, `sla_${splitOpsla[1]}`);
          } catch (erro) {
            console.log(erro);
          }

          await Attachment.handleOutgoingAttachment(
            context,
            path.resolve(__dirname, "..", "graphics", `sla_${splitOpsla[1]}`)
          );

          await this.newProcess(context);
          break;
        case "process_slaop":
          let split = context.activity.text.split(" ");
          try {
            await context.sendActivities([{ type: ActivityTypes.Typing }]);
            await context.sendActivity(`Informe a fila que deseja`);

            sla.action = true;
          } catch (erro) {
            console.log("Erro opsla" + error);
          }
          break;
        case "dialog_end":
          await context.sendActivities([{ type: ActivityTypes.Typing }]);
          await context.sendActivity(
            `Encerramento do atendimento com sucesso! Obrigado(a)`
          );
          break;
      } //end switch
    });
  }

  async run(context) {
    await super.run(context);

    // Save any state changes. The load happened during the execution of the Dialog.
    await this.conversationState.saveChanges(context, false);
    await this.userState.saveChanges(context, false);
  }

  static async fillOutUserProfile(flow, profile, turnContext) {
    const input = turnContext.activity.text;
    let result;

    await turnContext.sendActivity("Let's get started. What is your name?");
    flow.lastQuestionAsked = question.queue;

    await turnContext.sendActivity(question.queue);

    switch (flow.lastQuestionAsked) {
      // If we're just starting off, we haven't asked the user for any information yet.
      // Ask the user for their name and update the conversation flag.
      case question.none:
        await turnContext.sendActivity("Let's get started. What is your name?");
        flow.lastQuestionAsked = question.queue;
        break;
      case question.queue:
        result = this.validateName(input);
        if (result.success) {
          profile.name = result.name;
          await turnContext.sendActivity(
            `I have your name as ${profile.name}.`
          );
        } else {
          // If we couldn't interpret their input, ask them for it again.
          // Don't update the conversation flag, so that we repeat this step.
          await turnContext.sendActivity(
            result.message || "I'm sorry, I didn't understand that."
          );
          break;
        }
    }

    //   // If we last asked for their name, record their response, confirm that we got it.
    //   // Ask them for their age and update the conversation flag.
    //   case question.none:
    //     result = this.validateName(input);
    //     if (result.success) {
    //       profile.name = result.name;
    //       await turnContext.sendActivity(
    //         `I have your name as ${profile.name}.`
    //       );
    //     } else {
    //       // If we couldn't interpret their input, ask them for it again.
    //       // Don't update the conversation flag, so that we repeat this step.
    //       await turnContext.sendActivity(
    //         result.message || "I'm sorry, I didn't understand that."
    //       );
    //       break;
    //     }
    // }
  }

  // Validates name input. Returns whether validation succeeded and either the parsed and normalized
  // value or a message the bot can use to ask the user again.
  static validateName(input) {
    const name = input && input.trim();
    return name !== undefined
      ? { success: true, name: name }
      : {
          success: false,
          message: "Please enter a name that contains at least one character.",
        };
  }
}

module.exports.TeamsConversationBot = TeamsConversationBot;
