const fs = require("fs");

const path = require("path");

const { ActivityTypes } = require("botbuilder");

class AttachmentImages {
  getInlineAttachment(pathname) {
    let imageData = fs.readFileSync(pathname + '.png');
    let base64Image = Buffer.from(imageData).toString("base64");


    return {
      name: `${pathname}`,
      contentType: "image/png",
      contentUrl: `data:image/png;base64,${base64Image}`,
    }
  }

  async handleOutgoingAttachment(turnContext, pathname) {
    let reply = { type: ActivityTypes.Message };
      
    reply.attachments = [this.getInlineAttachment(pathname)];
    
    try{
      return await turnContext.sendActivity(reply);
    }catch(err){
      console.log(err)
    }
  }
}

module.exports = new AttachmentImages();
