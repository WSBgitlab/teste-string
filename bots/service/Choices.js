const MessagesProcess = require("./MessagesProcess");

const MessagesSender = require("./../SendMessages");

const commands = require("./../../JSON/config/cmd.json");

const Help = require("./../service/Help");

const ServiceQueue = require("./../service/ServicesQueue");

const { CardFactory, ActivityHandler, MessageFactory  } = require('botbuilder');

const path = require("path");

const screen = require("./Puppeteer");

const PhotoScreen = require("./Pupup");

class Choices extends MessagesProcess {
  constructor(context, message, membersNumber, next) {
    super(context, message, membersNumber);

    this.context = context;
    this.message = message;
    this.membersNumber = membersNumber;
    this.next = next;
  }
    
  async help(context,message){
    let options = [
      {
        type: 'messageBack',
        title: 'SLA',
        text: 'sla',
        displayText: 'sla'
      },
      {
        type: 'messageBack',
        title: 'PCR',
        text: 'pcr',
        displayText: 'pcr'
      },
      {
        type: 'messageBack',
        title: 'r2c3',
        text: 'r2c3',
        displayText: 'r2c3'
      }
    ];

    let messageCard = MessageFactory.attachment(
     CardFactory.heroCard(null, undefined, options, { text: message   })
    );

    await context.sendActivity(messageCard);

    return context.activity.text;
  }


  async newProcess(){
    let options = [
      {
          type:'messageBack',
          title:'Novo atendimento',
          text:'Novo atendimento',
          displayText:'Novo atendimento'
      }
    ]
    
    let messageCard = MessageFactory.attachment(
      CardFactory.heroCard(null, undefined, options, { text: 'Deseja um novo atendimento? ' })
    );

    try{
      await this.context.sendActivity(messageCard);
    }catch(err){
      console.log(err)
    }
  }

  async main(context) {
    // Validação se o usuário enviou um comando válido.
    const validate = commands.cmd.filter(
      (cmdFind) => cmdFind == this.txtSystem
    );
    
    const helpMessage = new Help(context);
    /*
    console.log(validate.length === 0 && this.membersNumber > 1,"validate.length === 0 && this.membersNumber > 1");
    console.log(validate.length === 0 && this.membersNumber === 1,"validate.length === 0 && this.membersNumber === 1");
    if (validate.length === 0 && this.membersNumber > 1) {
      return await this.help(context,helpMessage.sendHelpAll());
    }  else if(validate.length === 0 && this.membersNumber === 1){
      return await this.help(context,helpMessage.sendSingle()); 
    }*/
    
    let op = "";
    
    if(validate.length === 0){
      op = await this.help(context,helpMessage.sendSingle());
      return;
    }
    
    switch (this.txtSystem) {
      case "wso":
        await this.sendWso2();
        break;
      case "prints":
        await this.photoScreen();
        break;
      case "sla":
        let optionsSla = [
            {
              type:'messageBack',
              title:'sla',
              text:'slaop',
              displayText:'SLA'
            },
            {
              type:'messageBack',
              title:'Centro de Comando',
              text:'opcdm',
              displayText:'Centro de comando'
            }
          ];
          
          let messageCard = MessageFactory.attachment(
            CardFactory.heroCard(null, undefined, optionsSla, { text: 'Selecione um serviço :'    })
          )

          await context.sendActivity(messageCard);       
          return; 
        break;
      case 'pcr':
        let optionsPcr = [
          {
            type:'messageBack',
            title:'PCR HEALTH CHECK',
            text:'pcrhc',
            displayText:'HEALTH CHECK'
          },
          {
            type:'messageBack',
            title:'PCR Fila MQ',
            text:'pcrfilamq',
            displayText:'Fila MQ'
          },
          {
            type:'messageBack',
            title:'MQ Channel',
            text:'pcrmqchannel',
            displayText:'MQ Channel'
          },
        ]
        
        let messageCardPcr = MessageFactory.attachment(
          CardFactory.heroCard(null, undefined, optionsPcr, { text: 'Selecione um serviço sistema PCR :' })
        );
        
        await context.sendActivity(messageCardPcr);
        console.log("pcr",context.activity);
        return;
        break;
      case 'r2c3':
        let optionsRC = [
          {
              type:'messageBack',
              title:'R2C3 HEALTH CHECK',
              text:'r2c3hc',
              displayText:'HEALTH CHECK'
            
             }
        ]
        
        let messageCardRC = MessageFactory.attachment(
          CardFactory.heroCard(null,undefined,optionsRC, { text : 'Selecione um serviço sistema R2C3 :' })
        )
        
        await context.sendActivity(messageCardRC);
        break;
      case 'pcrhc':
        let urlhc = 'https://dashboard.cip-core.local:8443/d/opBSU8JMz/bot-pcr-health-check';
        
        let urlhc2 = 'https://dashboard.cip-core.local:8443/d/VjtY881Gk/bot-pcr-health-check2?refresh=1m&orgId=1';
      
        try{
          screen.init(context,urlhc,'pcrhc').then(response => {
            return;
          })
          
          screen.init(context,urlhc2,'pcrhc2').then(response => {
            return;
          })
        }catch(erro){
          console.log("Erro ao enviar o download");
        }

        break;
      case 'pcrfilamq':
        let urlPcrFila = 'https://dashboard.cip-core.local:8443/d/dY58vDmZk/enfileiramento_mq_msg_part_in?orgId=1&https:%2F%2Fdashboard.cip-core.local:8443%2Fd%2FdY58vDmZk%2Fenfileiramento_mq_msg_part_in%3ForgId=1';
        
        await context.sendActivity("Aguarde em instantes sua mensagem será carregada..");

        try{
          await screen.init(urlPcrFila,'pcrfilamq');
        }catch(erro){
          console.log("Erro ao realizar o download");
        }
        
        await MessagesSender.sendScreenGrafana(context,'pcrfilamq');
        
        await this.newProcess(context);
        break;
      case 'pcrmqchannel':
        let urlPcrChannel = 'https://dashboard.cip-core.local:8443/d/_rpTdZ7iz/pcr-mq-channel-performance?refresh=1m&orgId=1';

        await context.sendActivity("Aguarde em instantes sua mensagem será carregada..");

        try{
          await screen.init(context,urlPcrChannel,'pcrmqchannel');
        }catch(erro){
          console.log("Erro ao realizar o download");
        }

        await this.newProcess(context);
        break
      case 'r2c3hc':
        let urlR2c3HC = 'https://dashboard.cip-core.local:8443/d/DBnNz4FGz/r2c3-hext?refresh=1m&orgId=1';

        await context.sendActivity("Aguarde em instantes sua mensagem será carregada..");

        try{
          await screen.init(urlR2c3HC,'r2c3hc');
        }catch(erro){
          console.log("Erro ao realizar o download")
        }

        await MessagesSender.sendScreenGrafana(context,'r2c3hc');
        
        await this.newProcess(context);
        break
      case 'opcdm':
          //await messageProcess.centrodecomando();
        let url = `https://dashboard.cip-core.local:8443/d/L2b_kbfWz/sla_central_prd_centro_comando`;

        await context.sendActivity("Aguarde em instantes sua mensagem será carregada..");
        
        try{
          await screen.init(url,`cmd_centrodecomando`);
        }catch(erro){
          console.log("Erro ao realizar o download");
        }

        await MessagesSender.sendScreenGrafana(context,`cmd_centrodecomando`);
        
        await this.newProcess(context);
        break;
      case 'slaop':
        let split = context.activity.text.split(" ");
        
        if(split[1] !== undefined){
          // this.txtCommand -> antools

          // this.txtSystem -> slaop
          await context.sendActivity(`Aguarde um momento.`);
          
          await MessagesSender.sendQueueScreen(context,"sla",`${this.txtCommand}`);
          
          await this.newProcess(context);
          return;
        }
        
        try{
          await context.sendActivity(`Informe a fila que deseja, com o prefixo slaop:<br> <b>slaop fila</b> <br>`);
        }catch(erro){
            console.log("Erro opsla" + error);
        }
        
        break;
      default:
        await this.systems();
        break;
    }
  }
}

module.exports = Choices;
