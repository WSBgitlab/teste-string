const util = require("util");

const exec = util.promisify(require("child_process").exec);

const { TurnContext } = require("botbuilder");

const SendMessages = require("../SendMessages");

const path = require("path");


class MessagesProcess {
 /**
  * 
  * @param {TurnContext} context 
  * @param {string} message 
  */
  constructor(context, message, membersNumber) {
    this.message = message;
    this.context = context;
    this.inCommand = 1;
    this.inSystem = 0;
    this.msg = this.message.split(" ");
    this.membersNumber = membersNumber;
    
    // Quantidade de membros, irá definir o index de comandos e sistemas
    /**
     * 
     * Até o momento teremos 3, sendo eles:
     * 
     * sistema -> será representado o sistema que deseja receber a mensagem
     * comando -> será a representação do gráfico dentro do grafana.
     * filas -> Alguns comandos terão um terceiro atributo para indicar a fila do determinado gráfico.
     * 
     */    


    
    if (this.membersNumber >= 2){
      this.inSystem = 1;
      this.inCommand = 2;
    }
  }

  // sistemas

  async systems() {
    try{
      let message = this.message.split(" ");

      const result = await SendMessages.productSendMessage(
        this.context,
        message[this.inSystem],
        message[this.inCommand]
      );
      
      return result;
    }catch(error){
      throw Error("Erro ao processar systema, function System" + error);      
    }
  }
  
  async slaQueueScreen(){
    try{
      let message = this.message.split(" ");

      const result = await SendMessages.sendQueueScreen(
        this.context,
        message[this.inSystem],
        message[this.inCommand]
      );

      return result;
    }catch(erro){
      console.log("Erro ao processar screen",erro);
    }
  }
  

  async centrodecomando(){
    try{
      const result = await SendMessages.sendCenter(
        this.context,
        "centrodecomando",
        "centrodecomando"
      );

      return result;
    }catch(erro){
      console.log("Erro ao processar comando",erro);
    }

    return false;
  }

  // PhotoScreen com um único comando

  async photoScreen() {
    try {
      let message = this.message.split(" ");

      const result = await SendMessages.sendScreen(
        this.context,
        message[this.inSystem],
        message[this.inCommand]
      );

      return result;
    } catch (error) {
      throw Error(
        "Erro ao processar print screen, function photoScreen" + error
      );
    }
  }

  // Wso
  async sendWso2() {
    let comando = `xvfb-run --server-args="-screen 0,1024x768x24" wkhtmltoimage --window-status imdone --run-script 'window.setTimeout(function (){window.status = "imdone";},6000);' https://paineldealertas.cip-aws.com:444/PainelTV?UserID={00000000-0000-0000-0000-000000000000} ${path.resolve(
      __dirname,
      "..",
      "..",
      "graphics",
      "wso.png"
    )}`;

    try {
      await exec(comando);

      await AttachmentImages.handleOutgoingAttachment(this.context);

      return true;
    } catch (err) {
      console.log("Ocorreu erro ao enviar o gráfico wso2", err);
      return false;
    }
  }

  // Ajuda geral
  
  async help(){
    try {
      const result = await SendMessages.sendHelp();

      return result;
    } catch (error) {
      console.log("Erro ao enviar ajuda geral fun:help" + error)
    }
  }

  get sizeParams(){
    return this.message.length;
  }

  get txtSystem(){
    let message = this.message.split(" ");
    
    return message[this.inSystem];
  }

  get txtCommand(){
    let message = this.message.split(" ");

    return message[this.inCommand];
  }
}

module.exports = MessagesProcess;
