const path = require("path");

const dataHelp = require("./../../JSON/help/help.json");

class MessagesHelp {
  constructor(context){
      this.context = context;
  }
  
  sendSystemHelp(command) {
    try {
      const systemHelp = require(path.resolve(
        __dirname,
        "..",
        "..",
        "JSON",
        "sistemas",
        `cip_${command}.json`
      ));

      let text = "<br>Solicitação de ajuda sistema : " + command + "<br><br>";
      // Ordena o comando por ordem alfabética.
      text += this.sortWithCmd(systemHelp.data);
      
      return text;
    } catch (erro) {
      throw Error("Erro ao enviar mensagem de help! (SISTEMA)" + erro);
    }
  }
  
  sendSystemArrHelp(command){
    try{
      const array = require(path.resolve(
        __dirname,
        "..",
        "..",
        "JSON",
        "sistemas",
        `cip_${command}.json`
      ));
      
      let options = [];

      let ordena = array.data.sort(function (str, strcompare) {
        if (str.cmd > strcompare.cmd) {
          return 1;  
        }
        
        if (str.cmd < strcompare.cmd) {
          return -1; 
        }

        return 0;
      });
        
      ordena.map(item => options.push(item.cmd));
      console.log(options);
      console.log(typeof(options));
      return options;
    }catch(erro){
      throw Error("Erro ao gerar array mensagem de help ! (Sistema)" + erro);
    }
  }



  sendPrintHelp(){
    const prints = require(path.resolve(
      __dirname,
      "..",
      "..",
      "JSON",
      "prints",
      "cip_prints.json"
    ))
    

    let text = "<br>Solicitação de ajuda Prints <br><br>";

    text += this.sortWithCmd(prints.data);
    
    return text;
  }
  
  sendSingle(){
    let text = "<b>Bot Teams Grafana</b> <br><br>";
    

    text += `Olá ${this.context.activity.from.name}, seja bem vindo(a) <br><br>`;
    text += "Estamos no menu principal, por gentileza seguir as <b>instruções</b> do Bot Grafana<br><br>";
    text += "Favor escolher uma das opçoes abaixo : <br><br>";

    let ordena = dataHelp.help.sort(function (str, strcompare) {
      if (str.command > strcompare.command) {
        return 1;
      }
      if (str.command < strcompare.command) {
        return -1;
      }

      return 0;
    })

    return text; 
  }
  
  sendHelpAll() {
    let text = "<br><br> <b>Bot Teams Grafana</b> <br><br>";

    text += `Olá seja bem vindo ao help do bot grafana! <br><br> @grafana **sistema** **comando**<br>`;

    text += `<br>Exemplo de uso: @grafana pcr /h - Ajuda do sistema PCR<br>`;

    text += `<br>Utilize /h para solicitar ajuda dos sistemas<br>`;

    text += `<br>Lista de sistemas disponíveis<br>`;
    

    // Ordenando pela ajuda geral
    let ordena = dataHelp.help.sort(function (str, strcompare) {
      if (str.command > strcompare.command) {
        return 1;
      }
      if (str.command < strcompare.command) {
        return -1;
      }

      return 0;
    });

    ordena.map((data) => (text += `<b> - ${data.command} </b><br>`));

    return text;
  }
  
  // Ordena recebendo um array do sistema solicitado.
  sortWithCmd(array) {
    let text = "";

    let ordena = array.sort(function (str, strcompare) {
      if (str.cmd > strcompare.cmd) {
        return 1;
      }
      if (str.cmd < strcompare.cmd) {
        return -1;
      }

      return 0;
    });

    ordena.map(
      (data) => (text += `<b> - ${data.cmd}</b> ${data.help}<br>`)
    );

    return text;
  }
}

module.exports = MessagesHelp;
