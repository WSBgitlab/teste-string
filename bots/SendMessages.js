const { MessageFactory, TurnContext } = require("botbuilder");

const path = require("path");

const util = require("util");

const exec = util.promisify(require("child_process").exec);

const fs = require("fs");

const AttachmentImages = require("./service/AttachmentImages");

const MessagesHelp = require("./service/Help");

const screen = require("./service/Puppeteer");

class SendMessage {
  constructor() {
    this.help = "/h";
  }

  /**
   *
   * @param {TurnContext} context
   * @param {string} system
   * @param {string} command
   *
   * Será realizado o download no grafana e enviado ao usuário solicitado.
   *
   */
  async productSendMessage(context, system, command) { 
      // Instanciando classe help para quando essa função ser chamada
      // Responder a somente o contexto que solicitou-se em uma requisição.
      const messagesHelp = new MessagesHelp(context);

      if (command == undefined || command === this.help) {
        // Retornar o array de opções para os botões.
        return await context.sendActivity(`${messagesHelp.sendSystemHelp(system)}`);
      }

      const readJson = require(path.resolve(
        __dirname,
        "..",
        "JSON",
        "sistemas",
        `cip_${system}.json`
      ));
      
      const findCommand = readJson.data.filter((item) => item.cmd === command);
      
      if (findCommand.length === 0){
        await context.sendActivity(
          `<br> Comando inválido <br>${messagesHelp.sendSystemHelp(command)}`
        );
      }

      await context.sendActivity(
        `Aguarde um momento, esse processo pode demorar até 1 minuto.`
      );
      
      const resultSendGraphic = this.sendGraphic(
        context,
        findCommand[0].url,
        system,
        command
      );
      
      return resultSendGraphic;
  }

  /**
   *
   * @param {TurnContext} context
   * @param {string} system
   * @param {string} command
   */
  async sendScreen(context, system, command) {
    const dashs = require(path.resolve(__dirname,"..", "JSON", "prints","cip_prints.json"));
    
    const messagesHelp = new MessagesHelp(context);
    
    if (command === `${this.help}` || command === undefined)
      return await context.sendActivity(`${messagesHelp.sendPrintHelp()}`);

    let findCommand = dashs.data.filter((item) => item.cmd === command);
   
    await context.sendActivity(
      `Aguarde um momento, esse processo pode demorar até 1 minuto.`
    );

    await screen.init(`${findCommand[0].url}`, `${findCommand[0].cmd}`);

    try {
      await this.sendScreenGrafana(context, `${findCommand[0].cmd}`);
    } catch (err) {
      console.log("erro ao enviar printscreen!" + err);
    }
  }
  
  async sendQueueScreen(context,system,command){
    try{
      let url = `https://dashboard.cip-core.local:8443/d/6LIqFLPWz/sla_central_geral?refresh=1m&orgId=1&var-Queue=${command.toUpperCase()}`;
      

     await context.sendActivity("Aguarde em instantes sua mensagem será carregada..");
      
      await screen.init(context,url,`sla_${command}`);
      
       await this.sendScreenGrafana(context,`sla_${command}`);
      return true;
    }catch(erro){
      console.log("Erro ao enviar printscreen da fila " + erro);
    }
  }
  
  async sendCenter(context,system,command){  
    try{
      let url = `https://dashboard.cip-core.local:8443/d/L2b_kbfWz/sla_central_prd_centro_comando`;
      
      await context.sendActivity("Aguarde em instantes sua mensagem será carregada..");
      
      let response = await screen.init(url,`cmd_${command}`);
      
      await this.sendScreenGrafana(context,`cmd_${command}`);
      
      return true;
    }catch(erro){
      console.log("erro ao enviar printscreen Command Center", erro);
    }
  }

  async notFound(context) {
    const messageHelp = new MessageHelp(context);
    


    await context.sendActivity(
      MessageFactory.text(
        `Comando não encontrado com sucesso! ${messagesHelp.sendHelpAll()}`
      )
    );
  }

  /**
   *
   * @param {TurnContext} context
   * @param {string} url
   * @param {string} command
   * @param {string} graphic
   */
  async sendGraphic(context, url, command, graphic) {
    let pathname = path.resolve(__dirname,"..","graphics",`${command}_${graphic}`);
    
    await exec(
      `/bin/curl "${url}" -H "Authorization: Bearer ${process.env.TOKEN_GRAFANA}" --compressed --insecure > ${pathname}`
    );

    try {
      const sendImage = await AttachmentImages.handleOutgoingAttachment(context, pathname);

      return sendImage;
    } catch (err) {
      console.log("erro ao enviar imagem para o teams", err);
    }
  }

  /**
   *
   * @param {TurnContext} context
   * @param {string} command
   * @param {string} graphic
   */
  async sendDashboard(context, command, graphic) {
    // Ler arquivo que contém as urls
    let file = fs.readFileSync(
      path.resolve(__dirname, `graphics`, `${command}_${graphic}.txt`)
    );

    // Filtro para validar se o arquivo existe dentro de um arquivo de controle.
    let { stdout } = await exec(`
      cat ${path.resolve(__dirname)}/graphics/${command}_${graphic}.txt | wc -l
    `);

    // Quantidade de linhas que o arquivo encontrado contém.
    let qtdRow = parseInt(stdout);

    // Executar comando para realizar o download dos gráficos.
    const outputDownload = await exec(`
      cat ${path.resolve(
        __dirname
      )}/graphics/${command}_${graphic}.txt | while read x y z
        do
          echo $y
          curl "$x" -H "Authorization: Bearer ${
            process.env.TOKEN_GRAFANA
          }" --compressed --insecure > /opt/bot-cip/bot-teams-cip/graphics/${command}_"$z".png
          echo "============"
        done
    `);

    // envio para o bot
    for (let i = 0; i < qtdRow; ++i) {
      await this.handleOutgoingAttachment(
        context,
        path.resolve(
          __dirname,
          "..",
          "graphics",
          `${command}_${graphic}${i + 1}`
        )
      );
    }

    return true;
  }

  async sendHelp(context) {
    try {
            
      await context.sendActivity(  
        MessageFactory.text(`${MessagesHelp.sendHelpAll()}`)
      );
    } catch (err) {
      console.log("Erro ao enviar help sendhelp:sendmessages" + err);
    }
  }
}

module.exports = new SendMessage();
