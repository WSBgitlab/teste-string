// Arquivo de gráficos dedicados;
const graphSccDedicates = require("./../JSON/cip_ctc_graph.json");

// arquivo de gráficos para válidação
const graphSccGroups = require("./../JSON/cip_ctc_grp_graph.json");

// Help
if (msg[2] === `${this.help}`) {
  try {
    await context.sendActivity(`${help.sendHelpScc()}`);
    return true;
  } catch (err) {
    console.log("erro ao executar o help ctc", err);
  }
}

let isSccDedicate = graphSccDedicates.comandos.filter(
  (item) => item.shortName === msg[2]
);

let isSccGroup = graphSccGroups.dashboards.filter(
  (item) => item.shortName === msg[2]
);

if (isSccDedicate.length == 0 && isSccGroup.length == 0) {
  return await context.sendActivity(
    `<br> Comando inválido <br>${help.sendHelpScc()}`
  );
}

await context.sendActivity(`Aguarde um momento.`);

if (isSccDedicate != 0) {
  await this.sendGraphics(
    context,
    isSccDedicate[0].pathUrl,
    msg[1],
    msg[2]
  );
} else if (isSccGroup != 0) {
  await this.sendDashboard(context, msg[1], msg[2]);
}